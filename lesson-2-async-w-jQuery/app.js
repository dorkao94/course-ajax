const YOUR_APPLICATION_ID='4ab6db339b5897c5a9af26ca6a65b11ef1ac72af287b1a0d0b7388f1ff0b9062';
const NEWYORKTIMES_API = 'f2081e0dfd0a44948b3b1a2534941a4e';


function requestError(e, part,responseContainer) {
    console.log(e);
    responseContainer.insertAdjacentHTML('beforeend', `<p class="network-warning">Oh no! There was an error making a request for the ${part}.</p>`);
}

(function () {
    const form = document.querySelector('#search-form');
    const searchField = document.querySelector('#search-keyword');
    let searchedForText;
    const responseContainer = document.querySelector('#response-container');

    form.addEventListener('submit', function (e) {
        e.preventDefault();
        responseContainer.innerHTML = '';
        searchedForText = searchField.value;

        $.ajax({
                url: `https://api.unsplash.com/search/photos?page=1&query=${searchedForText}`,
                headers: {Authorization: 'Client-ID ' +YOUR_APPLICATION_ID}
            }).done(function(data) {
                let htmlContent = '';
                const firstImage = data.results[0];

                if (firstImage) {
                    htmlContent = `<figure>
                        <img src="${firstImage.urls.small}" alt="${searchedForText}">
                        <figcaption>${searchedForText} by ${firstImage.user.name}</figcaption>
                    </figure>`;
                } else {
                    htmlContent = 'Unfortunately, no image was returned for your search.'
                }

                responseContainer.insertAdjacentHTML('afterbegin', htmlContent);
        });

        $.ajax({
                url: `http://api.nytimes.com/svc/search/v2/articlesearch.json?api-key=${NEWYORKTIMES_API}&q=${searchedForText}`,
                context: document.body
            }).done(function(data) {
                let htmlContent = '';
                if(data.response && data.response.docs && data.response.docs.length > 1){
                        htmlContent = '<ul>' + data.response.docs.map(article => `<li class="article">
                        <h2><a href="${article.web_url}">${article.headline.main}</a></h2>
                        <p>${article.snippet}</p>
                        </li>`).join('')+ '</ul>';
                }else{
                        htmlContent = '<div class="error-no-articles" >No articles available.</div>';
                }
                responseContainer.insertAdjacentHTML('beforeEnd', htmlContent);
        });
        
    });
})();
