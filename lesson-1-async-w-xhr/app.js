(function () {
    const YOUR_APPLICATION_ID='4ab6db339b5897c5a9af26ca6a65b11ef1ac72af287b1a0d0b7388f1ff0b9062';
    const NEWYORKTIMES_API = 'f2081e0dfd0a44948b3b1a2534941a4e';

    const form = document.querySelector('#search-form');
    const searchField = document.querySelector('#search-keyword');
    let searchedForText = 'hippos';
    const responseContainer = document.querySelector('#response-container');

    form.addEventListener('submit', (e)  => {
        e.preventDefault();
        responseContainer.innerHTML = '';
        searchedForText = searchField.value;
        const unsplashRequest = new XMLHttpRequest(); 
        unsplashRequest.open('GET', `https://api.unsplash.com/search/photos?page=1&query=${searchedForText}`);
        unsplashRequest.setRequestHeader('Authorization', 'Client-ID '+YOUR_APPLICATION_ID);
        // unsplashRequest.onload = addImage;
        unsplashRequest.onreadystatechange = () => {//Call a function when the state changes.
            if(unsplashRequest.readyState == XMLHttpRequest.DONE && unsplashRequest.status == 200) {
                // Request finished. Do processing here.
                console.log('everythings cool') 
                // console.log(unsplashRequest.responseText)
                let htmlContent = ''; 
                    // debugger; 
                    
                const data = JSON.parse(unsplashRequest.responseText);
                // const firstFiveImage = data.results.slice(0, 5);
                if(data && data.results && data.results[0]){
                        const firstImage = data.results[0];
                        htmlContent = `<figure>
                            <img src="${firstImage.urls.regular}" alt="${searchedForText}" >
                            <figcaption>${searchedForText} by ${firstImage.user.name}</figcaption>
                                    </figure>`;
                }else{
                        htmlContent = '<div class="error-no-image" >No images available.</div>';
                }
                // responseContainer.innerHTML = htmlContent;'
                responseContainer.insertAdjacentHTML('afterBegin', htmlContent);
                // console.log(htmlContent)
                
                 
                // let o=JSON.parse(unsplashRequest.responseText);
                // let desc = "";
                // for(let i of o.results){
                //     desc += i.description + "<br>";
                // }
                // console.log(desc)
                // responseContainer.innerHTML = desc;
            }
        }
        unsplashRequest.send();

        const articleRequest = new XMLHttpRequest();
        articleRequest.onreadystatechange = () => {
            if(articleRequest.readyState == XMLHttpRequest.DONE && articleRequest.status == 200) {
                let htmlContent = '';
                // console.log(articleRequest.responseText)
                const data = JSON.parse(articleRequest.responseText);
                if(data.response && data.response.docs && data.response.docs.length > 1){
                    htmlContent = '<ul>' + data.response.docs.map(article => `<li class="article">
                    <h2><a href="${article.web_url}">${article.headline.main}</a></h2>
                    <p>${article.snippet}</p>
                    </li>`).join('')+ '</ul>';
                }else{
                    htmlContent = '<div class="error-no-articles" >No articles available.</div>';
                }
                responseContainer.insertAdjacentHTML('beforeEnd', htmlContent);
            }
        };
        articleRequest.onerror = (err) => {
            console.log('article error');
        };
        articleRequest.open('GET', `http://api.nytimes.com/svc/search/v2/articlesearch.json?api-key=${NEWYORKTIMES_API}&q=${searchedForText}`);
        articleRequest.setRequestHeader('Access-Control-Allow-Headers', '*');
        // articleRequest.setRequestHeader('Authorization', 'Api-key '+NEWYORKTIMES_API); 
        articleRequest.send();
    });
})();
